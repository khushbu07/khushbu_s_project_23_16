
view: sql_runner_query {
  derived_table: {
    sql: SELECT
          (DATE(CONVERT_TZ(orders.created_at ,'UTC','America/Los_Angeles'))) AS `orders.created_date`,
          orders.status  AS `orders.status`,
          order_items.order_id  AS `order_items.order_id`,
          order_items.sale_price  AS `order_items.sale_price`,
          COUNT(DISTINCT orders.id ) AS `orders.count`
      FROM demo_db.order_items  AS order_items
      LEFT JOIN demo_db.orders  AS orders ON order_items.order_id = orders.id
      WHERE
      {% condition active %} orders.status {% endcondition %}
      GROUP BY
          1,
          2,
          3,
          4
      ORDER BY
          1 DESC
      LIMIT 500 ;;
  }


  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: orders_created_date {
    type: date
    sql: ${TABLE}.`orders.created_date` ;;
  }
filter: active {
  type: string
  suggest_dimension: orders_status
}
  dimension: orders_status {
    type: string
    sql: ${TABLE}.`orders.status` ;;
  }

  dimension: order_items_order_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.`order_items.order_id` ;;
  }

  dimension: order_items_sale_price {
    type: number
    sql: ${TABLE}.`order_items.sale_price` ;;
  }

  dimension: orders_count {
    type: number
    sql: ${TABLE}.`orders.count` ;;
  }

  set: detail {
    fields: [
        orders_created_date,
  orders_status,
  order_items_order_id,
  order_items_sale_price,
  orders_count
    ]
  }
}
