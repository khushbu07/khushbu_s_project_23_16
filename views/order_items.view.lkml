view: order_items {
  sql_table_name: demo_db.order_items ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: inventory_item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.inventory_item_id ;;
  }
  dimension: order_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.order_id ;;
  }
  dimension: phone {
    type: string
    sql: ${TABLE}.phone ;;
  }
  dimension: phones {
    type: string
    sql: ${TABLE}.phones ;;
  }
  dimension_group: returned {
    type: time
    timeframes: [raw, time, date, week, month, quarter, year]
    sql: ${TABLE}.returned_at ;;
  }
  dimension: sale_price {
    type: number
    sql: ${TABLE}.sale_price ;;
  }

  parameter: activity {
    type: string
    default_value: "All"
    allowed_value: {
      label: "All"
      value: "All"
    }
    allowed_value: {
      label: "1:1 Session"
      value: "Calls"
    }
    allowed_value: {
      label: "Blog"
      value: "Blog Read"
    }
    allowed_value: {
      label: "Group Session"
      value: "Group Session"
    }
    allowed_value: {
      label: "Message"
      value: "Message"
    }
  }

  dimension: test123 {
    sql: CASE
WHEN {% parameter activity %} = "'Group Session'"
THEN ${returned_date}
WHEN {% parameter activity %} = "'Calls'"
THEN ${returned_week}
WHEN {% parameter activity %} = "'Blog Read'"
THEN ${returned_month}
WHEN {% parameter activity %} = "'Message'"
THEN ${returned_quarter}
WHEN {% parameter activity %} = "'All'"
THEN ${returned_year}
ELSE
${sale_price}
END;;
  }

  measure: count {
    type: count
    drill_fields: [id, orders.id, inventory_items.id]
  }
}
